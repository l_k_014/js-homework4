let number1 = prompt('Enter number 1');
let number2 = prompt('Enter number 2');
let operation = prompt('Enter the needed math operation: +, -, *, /');

function mathOperation(number1, number2, operation){

number1 = Number(number1);
number2 = Number(number2);

let number1Boolean = Boolean(number1);
let number2Boolean = Boolean(number2);

if (number1Boolean == false){
    alert('Number 1 is not a number!');
}

if (number2Boolean == false){
    alert('Number 2 is not a number!');
}


switch (operation){
    case '+':
        return (number1 + number2);
    break;
    
    case '-':
        return (number1 - number2);  
    break;
    
    case '/':
        return (number1 / number2);
    break;

    case '*':
        return (number1 * number2);
    break;

    default:
        alert('Impossible operation!')
    break;
}
}

console.log(mathOperation(number1, number2, operation));
/* 
1. Функції у программувані потрібні задля того, щоб зменшити розмір коду та не повторювати його багато разів.
2. Функція, це як коробка, в яку ми кладемо аргумент, вона виконує певні дії над цим аргументом і видає результат
операцій, що відбувалися у ній. Тому нам потрібно дати аргумент для того, щоб функція виконувалася.
3. Return потрібен для того, щоб повернути значення функції. Тобто необов'язково потрібно у самій функції просити вивести її результат
на екран. Ми їй можемо надати значення, вона обрахує їх та результат цих операцій можна використовувати надалі у обчисленнях.

*/

